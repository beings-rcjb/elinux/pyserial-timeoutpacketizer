from setuptools import setup

setup(
    name='pyserial-timeoutpacketizer',
    # version='0.0.1',
    # description='Packetizer using timeouts for pyserial'
    packages=['timeoutpacketizer'],
    install_requires=['pyserial', 'pyserial-trt']
)
