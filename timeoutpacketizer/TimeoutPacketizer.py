import serial.threaded


class TimeoutPacketizer(serial.threaded.Protocol):

    # Implementation borrowed from serial.threaded.Packetizer
    def __init__(self):
        self.buffer = bytearray()
        self.transport = None

    def connection_made(self, transport):
        """Store transport"""
        self.transport = transport

    def connection_lost(self, exc):
        """Forget transport"""
        self.transport = None
        del self.buffer[:]
        super(TimeoutPacketizer, self).connection_lost(exc)

    def data_received(self, data):
        """Buffer received data"""
        self.buffer.extend(data)

    def data_timeout(self, data):
        """Packet terminated, call handle_packet"""
        self.buffer.extend(data)
        if self.buffer:
            self.handle_packet(self.buffer)

    def handle_packet(self, packet):
        """Process packets - to be overridden by subclassing"""
        raise NotImplementedError(
            'please implement functionality in handle_packet')

    def write(self, data):
        self.transport.write(data)
